//
// Created by tim on 16/08/2021.
//
#include <cstring>
#include <iostream>
#include <endian.h>
#include <boost/format.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "dataping_wire.hpp"

#ifndef DATAPING_WIRE_CPP
#define DATAPING_WIRE_CPP

std::vector<uint8_t> DataPingResponseV2::serialize() {
    std::vector<uint8_t> body;
    size_t i = body.size();
    body.resize(i + sizeof(this->mVersion));
    int32_t little_version = htole32(this->mVersion);
    std::memcpy(body.data() + i, &little_version, sizeof(this->mVersion));
    i = body.size();
    body.resize(i + sizeof(this->mType));
    int32_t little_type = htole32((int32_t) this->mType);
    std::memcpy(body.data() + i, &little_type, sizeof(this->mVersion));
    i = body.size();
    body.resize(i + sizeof(this->mSeq));
    uint16_t little_seq = htole16(this->mSeq);
    std::memcpy(body.data() + i, &little_seq, sizeof(this->mSeq));
    i = body.size();
    body.resize(i + LATENCYGG_SIG_LEN - 1);
    std::memcpy(body.data() + i, &(this->mSignature), LATENCYGG_SIG_LEN - 1);
    i = body.size();
    char pad[7] = "\x00\x00\x00\x00\x00\x00";
    body.resize(i + sizeof(pad) - 1);
    std::memcpy(body.data() + i, pad, sizeof(pad) - 1);
    i = body.size();
    body.resize(i + sizeof(this->mTimestamp));
    uint64_t little_ts = htole64(this->mTimestamp);
    std::memcpy(body.data() + i, &little_ts, sizeof(this->mTimestamp));
    return body;
}

std::shared_ptr<DataPingRequestV2> DataPingRequestV2::deserialize(std::vector<uint8_t> &aData) {
    if (aData.size() != 112) {
        throw std::invalid_argument((boost::format("wrong size (%1%)") % aData.size()).str().c_str());
    }
    std::shared_ptr<DataPingRequestV2> packet = std::make_shared<DataPingRequestV2>();
    size_t i = aData.size() - sizeof(timestamp_millisec_t);
    uint64_t little_ts;
    std::memcpy(&little_ts, aData.data() + i, sizeof(timestamp_millisec_t));
    packet->mTimestamp = le64toh(little_ts);
    aData.resize(i);
    i -= 6; // remove pad
    aData.resize(i);
    i -= (LATENCYGG_IDENT_LEN - 1);  // copy all bytes of sig but leaving space for null char at the end
    std::memcpy(&(packet->mIdent), aData.data() + i, LATENCYGG_IDENT_LEN - 1);
    aData.resize(i);
    i -= sizeof(uint16_t);
    uint16_t little_seq;
    std::memcpy(&little_seq, aData.data() + i, sizeof(uint16_t));
    packet->mSeq = le16toh(little_seq);
    aData.resize(i);
    i -= sizeof(data_ping_request_type_t);
    int32_t little_type;
    std::memcpy(&little_type, aData.data() + i, sizeof(data_ping_request_type_t));
    packet->mType = (data_ping_request_type_t) le32toh(little_type);
    aData.resize(i);
    i -= sizeof(int32_t);
    int32_t little_version;
    std::memcpy(&little_version, aData.data() + i, sizeof(int32_t));
    packet->mVersion = le32toh(little_version);
    aData.resize(i);
    return packet;
}


std::shared_ptr<DataPingRequestV3> DataPingRequestV3::deserialize(std::vector<uint8_t> &aData) {
    if (aData.size() != 192) {
        throw std::invalid_argument((boost::format("wrong size (%1%)") % aData.size()).str().c_str());
    }
    std::shared_ptr<DataPingRequestV3> packet = std::make_shared<DataPingRequestV3>();
    size_t i = aData.size() - sizeof(timestamp_millisec_t);
    uint64_t little_ts;
    std::memcpy(&little_ts, aData.data() + i, sizeof(timestamp_millisec_t));
    packet->mTimestamp = le64toh(little_ts);
    aData.resize(i);
    i -= 8; // remove pad
    aData.resize(i);
    i -= (LATENCYGG_TOKEN_LEN - 1);  // copy all bytes of sig but leaving space for null char at the end
    std::memcpy(&(packet->mToken), aData.data() + i, LATENCYGG_TOKEN_LEN - 1);
    aData.resize(i);
    i -= 6; // remove pad
    aData.resize(i);
    i -= sizeof(uint16_t);
    uint16_t little_seq;
    std::memcpy(&little_seq, aData.data() + i, sizeof(uint16_t));
    packet->mSeq = le16toh(little_seq);
    aData.resize(i);
    i -= sizeof(data_ping_request_type_t);
    int32_t little_type;
    std::memcpy(&little_type, aData.data() + i, sizeof(data_ping_request_type_t));
    packet->mType = (data_ping_request_type_t) le32toh(little_type);
    aData.resize(i);
    i -= sizeof(int32_t);
    int32_t little_version;
    std::memcpy(&little_version, aData.data() + i, sizeof(int32_t));
    packet->mVersion = le32toh(little_version);
    aData.resize(i);
    return packet;
}



std::shared_ptr<DataPingRequestContainer> DataPingRequestContainer::deserialize(std::vector<uint8_t> &aData) {
    std::shared_ptr<DataPingRequestContainer> packet = std::make_shared<DataPingRequestContainer>();
    int32_t little_version;
    std::memcpy(&little_version, aData.data(), sizeof(int32_t));
    packet->mVersion = le32toh(little_version);
    if (packet->mVersion == 2) {
        packet->dpv2 = DataPingRequestV2::deserialize(aData);
    } else if (packet->mVersion == 3) {
        packet->dpv3 = DataPingRequestV3::deserialize(aData);
    } else {
        throw std::invalid_argument("bad version");
    }
    return packet;
}

timestamp_millisec_t DataPingRequestContainer::timestamp() {
    timestamp_millisec_t result;
    if (this->mVersion == 2) {
        result = this->dpv2->mTimestamp;
    } else {
        result = this->dpv3->mTimestamp;
    }
    return result;
}

char * DataPingRequestContainer::identOrToken() {
    char * result;
    if (this->mVersion == 2) {
        result = this->dpv2->mIdent;
    } else {
        result = this->dpv3->mToken;
    }
    return result;
}

uint16_t DataPingRequestContainer::seq() {
    uint16_t result;
    if (this->mVersion == 2) {
        result = this->dpv2->mSeq;
    } else {
        result = this->dpv3->mSeq;
    }
    return result;
}

data_ping_request_type_t DataPingRequestContainer::type() {
    data_ping_request_type_t result;
    if (this->mVersion == 2) {
        result = this->dpv2->mType;
    } else {
        result = this->dpv3->mType;
    }
    return result;
}

int32_t DataPingRequestContainer::version() {
    return this->mVersion;
}

std::shared_ptr<DataPingTokenV1> DataPingTokenV1::decode(unsigned char * aData) {
    std::shared_ptr<DataPingTokenV1> token = std::make_shared<DataPingTokenV1>();
    std::vector<uint8_t> buffer;
    buffer.resize(LATENCYGG_TOKEN_DECRYPTED_LEN);
    std::memcpy(buffer.data(), aData, LATENCYGG_TOKEN_DECRYPTED_LEN);
    std::string token_str = std::string(buffer.begin(), buffer.end());
    size_t end_pos = token_str.find('\0');
    token_str.erase(end_pos);
    std::string delimiter = "/";
    size_t pos = 0;
    if ((pos = token_str.find(delimiter)) != std::string::npos) {
        std::string timestamp_str = token_str.substr(0, pos);
        std::vector<std::string> tz_delimiters = {"Z", "+", "-"};
        size_t t_pos = token_str.find("T");
        for (const auto& tz_delim : tz_delimiters) {
            size_t tz_pos = 0;
            if (((tz_pos = token_str.find(tz_delim)) != std::string::npos) && (tz_pos > t_pos)) {
                timestamp_str = timestamp_str.substr(0, tz_pos);
            }
        }
        boost::posix_time::ptime posix_time = boost::posix_time::from_iso_extended_string(timestamp_str);
        token->mTimestamp = boost::posix_time::to_time_t(posix_time) * 1000;
        token_str.erase(0, pos + delimiter.length());
        token->mIp = token_str;
    } else {
        throw std::invalid_argument("invalid token");
    }
    return token;
}

#endif // DATAPING_WIRE_CPP


