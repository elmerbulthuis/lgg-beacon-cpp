//
// Created by tim on 28/09/2021.
//

#ifndef CPP_API_CLIENT_H
#define CPP_API_CLIENT_H

#include <string>
#include <memory>
#include <curl/curl.h>
#include "udp_server.h"

namespace LatencyGG::Beacon {

    class ApiClient {
        std::string mToken;
        std::string mApi;
        std::string mUuid;
        uint32_t mPushPeriod;
        std::shared_ptr<std::mutex> mCompleteClaim;
        std::shared_ptr<std::unordered_multimap<timestamp_millisec_t, std::shared_ptr<DataPingState>>> mComplete;
        CURL *mCurl;
        std::thread mThread;
        std::mutex mLock;
        bool mAlive = false;
    public:
        ApiClient(std::string &aApi, std::string &aToken, uint32_t &aPushPeriod,
                  std::shared_ptr<std::unordered_multimap<timestamp_millisec_t, std::shared_ptr<DataPingState>>> &aComplete,
                  std::shared_ptr<std::mutex> &aCompleteClaim) : mToken(aToken), mApi(aApi), mPushPeriod(aPushPeriod),
                                                                 mComplete(aComplete), mCompleteClaim(aCompleteClaim) {
            curl_global_init(CURL_GLOBAL_DEFAULT);
            mCurl = curl_easy_init();
        }

        ~ApiClient() {
            curl_global_cleanup();
        }

        void registerBeacon(std::string &location, std::string &provider, std::string &ipv4, std::string &ipv6, std::string publickey_b64);

        void job();

        void run();

        void kill();
    };

}

#endif //CPP_API_CLIENT_H
