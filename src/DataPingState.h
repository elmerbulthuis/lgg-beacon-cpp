//
// Created by tim on 26/09/2021.
//

#include <string>
#include <nlohmann/json.hpp>
#include "dataping_wire.hpp"

#ifndef CPP_DATAPINGSTATE_H
#define CPP_DATAPINGSTATE_H

namespace LatencyGG::Beacon {

    enum data_ping_state_t {
        eC0 = 0, eS0, eS1, eComplete
    };

    struct DataPingState {
        std::string mIp;
        std::string mPub;
        std::string mIdent;
        uint8_t mSeq = 0;
        uint8_t mVersion = 0;
        data_ping_state_t mState = eC0;
        timestamp_millisec_t mTimestampsProbe[3] = {0, 0, 0};
        timestamp_millisec_t mTimestampsBeacon[3] = {0, 0, 0};

        nlohmann::json AsJSON();
    };

}
#endif //CPP_DATAPINGSTATE_H
