//
// Created by tim on 26/09/2021.
//

#include "DataPingState.h"
#include <boost/format.hpp>

using namespace LatencyGG::Beacon;

nlohmann::json DataPingState::AsJSON() {
    nlohmann::json jdata;
    nlohmann::json jraw;

    std::vector<timestamp_millisec_t> rtts;
    for (int i = 0; i < 3; i++) {
        jraw[(boost::format("timestamp_c%1%") % i).str()] = this->mTimestampsProbe[i];
        jraw[(boost::format("timestamp_s%1%") % i).str()] = this->mTimestampsBeacon[i];
        if (i > 0) {
            rtts.push_back(mTimestampsProbe[i] - mTimestampsProbe[i - 1]);
            rtts.push_back(mTimestampsBeacon[i] - mTimestampsBeacon[i - 1]);
        }
    }
    jdata["type"] = "udp-data";
    jdata["version"] = this->mVersion;
    jdata["ip"] = this->mIp;
    jdata["pub"] = this->mPub;
    timestamp_millisec_t max_elm = *max_element(rtts.begin(), rtts.end());
    if (max_elm > (std::numeric_limits<uint64_t>::max() / rtts.size())) {
        throw std::out_of_range("nonsensical timestamp found");
    }
    timestamp_millisec_t sum = std::accumulate(rtts.begin(), rtts.end(), (timestamp_millisec_t) 0);
    auto rtt = sum / rtts.size();
    auto square_diff = [rtt](timestamp_millisec_t a, timestamp_millisec_t b) {
        return a + (timestamp_millisec_t) std::pow(b - rtt, 2);
    };
    timestamp_millisec_t variance = std::accumulate(rtts.begin(), rtts.end(), 0, square_diff) / (timestamp_millisec_t)rtts.size();
    auto stddev = (timestamp_millisec_t) std::sqrt(variance);
    jdata["rtt"] = rtt;
    jdata["stddev"] = stddev;
    jdata["raw"] = jraw;
    return jdata;
}