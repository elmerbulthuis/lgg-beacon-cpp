//
// Created by tim on 24/09/2021.
//

#ifndef CPP_DATAPING_H
#define CPP_DATAPING_H


#include <vector>
#include <cstdint>
#include <mutex>
#include <memory>
#include <boost/asio/ip/udp.hpp>
#include "dataping_wire.hpp"
#include "DataPingState.h"

namespace LatencyGG::Beacon {

    using boost::asio::ip::udp;
    using data_ping_ts = std::chrono::milliseconds;

    class DataPing {

        std::mutex mLock;
        bool mAlive;
        std::shared_ptr<std::mutex> mSocketClaim;
        std::shared_ptr<udp::socket> mSocket;
        std::shared_ptr<std::mutex> mCompleteClaim;
        std::shared_ptr<std::mutex> mIncompleteClaim;
        std::shared_ptr<std::unordered_multimap<timestamp_millisec_t, std::shared_ptr<DataPingState>>> mComplete;
        std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<DataPingState>>> mIncomplete;
        uint8_t *mSigPk;
        uint8_t *mSigSk;
        uint8_t *mEncPk;
        uint8_t *mEncSk;
    public:
        DataPing(std::shared_ptr<udp::socket> &aSocket, std::shared_ptr<std::mutex> &aSocketClaim,
                 std::shared_ptr<std::unordered_multimap<timestamp_millisec_t, std::shared_ptr<DataPingState>>> &aComplete,
                 std::shared_ptr<std::mutex> &aCompeteClaim,
                 std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<DataPingState>>> &aIncomplete,
                 std::shared_ptr<std::mutex> &aIncompeteClaim,
                 uint8_t *aSigPk, uint8_t *aSigSk, uint8_t *aEncPk, uint8_t *aEncSk) : mAlive(false), mSocketClaim(aSocketClaim), mSocket(aSocket),
                                                                           mComplete(aComplete), mCompleteClaim(aCompeteClaim),
                                                                           mIncomplete(aIncomplete), mIncompleteClaim(aIncompeteClaim),
                                                                           mSigPk(aSigPk), mSigSk(aSigSk), mEncPk(aEncPk), mEncSk(aEncSk) {}

        void run();

        std::vector<uint8_t> datagramReceived(std::shared_ptr<DataPingRequestContainer> &request, udp::endpoint &endpoint);

        void kill();

        void validateDataPing(std::shared_ptr<DataPingRequestContainer> &dp, std::chrono::milliseconds &aCurrentTs, std::string aSourceIp);
        static data_ping_ts now();
    };

}
#endif //CPP_DATAPING_H
