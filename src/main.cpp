#include <iostream>
#include <chrono>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include "udp_server.h"
#include "api_client.h"
#include "base64.h"

using namespace std::chrono;
using boost::asio::ip::udp;
namespace po = boost::program_options;
using namespace LatencyGG::Beacon;

std::function<void(int)> sighandler = NULL;

extern "C" {
void c_handler(int i);
}

void c_handler(int i) {
    sighandler(i);
}

void signal_handler(const std::shared_ptr<UDPServer> &udpserver, const std::shared_ptr<ApiClient> &pusher, int signum) {
    udpserver->kill();
    pusher->kill();
    exit(0);
}

int main(int argc, char **argv) {
    po::options_description desc("Usage: beacon [OPTIONS] \n\n Options: ");
    desc.add_options()
            ("help", "Show this message and exit.")
            ("api", po::value<std::string>(), "The URL of the Beacon API (e.g. https://beacon.apis.latency.gg)")
            ("location", po::value<std::string>(),
             "The provider's name for the location that the beacon is hosted in (e.g. eu-west-1a)")
            ("provider", po::value<std::string>(), "The name of the hosting provider (e.g. maxihost)")
            ("ipv4", po::value<std::string>(), "The public IPv4 address of the beacon (e.g. 203.0.113.3)")
            ("ipv6", po::value<std::string>(), "The public IPv6 address of the beacon (e.g. 2001:db8::2021:6a4f:1)")
            ("token", po::value<std::string>(), "The Beacon API authentication token (Deprecated)")
            ("username", po::value<std::string>(), "The Beacon API authentication username")
            ("password", po::value<std::string>(), "The Beacon API authentication password")
            ("push_period", po::value<uint32_t>(), "The number of seconds between pushes");
    po::variables_map vm;
    po::store(po::parse_environment(desc, "BEACON_"), vm);
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }
    if (vm.count("location") * vm.count("provider") * vm.count("ipv4") * vm.count("api") == 0) {
        std::cout << desc << std::endl;
        return 0;
    }
    std::string location = vm["location"].as<std::string>();
    std::string provider = vm["provider"].as<std::string>();
    std::string ipv4 = vm["ipv4"].as<std::string>();
    std::string ipv6;
    if (vm.count(ipv6)) {
        ipv6 = vm["ipv6"].as<std::string>();
    }
    std::string api_url = vm["api"].as<std::string>();
    if (vm.count("token") + vm.count("username") * vm.count("password") > 1) {
        std::cout << desc << std::endl;
        std::cout << "Cannot use Bearer and BasicAuth at the same time" << std::endl;
        return 1;
    }
    std::string token;
    if (vm.count("token")) {
        std::cout << "WARNING: Use of Bearer Token is deprecated" << std::endl;
        token = (boost::format("Bearer %1%") % vm["token"].as<std::string>()).str();
    }
    else if (vm.count("username") + vm.count("password") == 2) {
        std::string username = vm["username"].as<std::string>();
        std::string password = vm["password"].as<std::string>();
        token = (boost::format("Basic %1%") % encode64((boost::format("%1%:%2%") % username %  password).str())).str();
    }
    else {
        std::cout << desc << std::endl;
        std::cout << "No valid authentication method found" << std::endl;
        return 1;
    }
    uint32_t push_period = 10;
    if (vm.count("push_period")) {
        push_period = vm["push_period"].as<uint32_t>();
    }
    boost::asio::io_service io_service;
    std::shared_ptr<udp::socket> s1 = std::make_shared<udp::socket>(io_service, udp::endpoint(udp::v4(), 9998));
    std::shared_ptr<udp::socket> s2 = std::make_shared<udp::socket>(io_service, udp::endpoint(udp::v6(), 9999));
    auto server = std::make_shared<UDPServer>(s1, s2, std::thread::hardware_concurrency() - 1);
    std::this_thread::sleep_for(5s);
    auto pusher = std::make_shared<ApiClient>(api_url, token, push_period, server->mComplete, server->mCompleteClaim);
    sighandler = std::bind(&signal_handler, server, pusher, std::placeholders::_1);
    signal(SIGTERM, c_handler);
    pusher->registerBeacon(location, provider, ipv4, ipv6, server->getPublicKey());
    pusher->run();
    server->serveForever();
    return 0;
}
