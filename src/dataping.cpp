//
// Created by tim on 24/09/2021.
//

#include <chrono>
#include <iostream>
#include <mutex>
#include <boost/format.hpp>
#include <sodium.h>
#include "dataping.h"

#define MAX_MTU 1500

using namespace LatencyGG::Beacon;
using namespace std::chrono_literals;

void DataPing::run() {
    mAlive = true;
    while (true) {
        udp::endpoint remote_endpoint;
        std::vector<uint8_t> request;
        request.resize(MAX_MTU);
        size_t bytes_recv;

        {
            std::lock_guard<std::mutex> lock(*mSocketClaim);
             bytes_recv = mSocket->receive_from(boost::asio::buffer(request.data(), MAX_MTU),
                                                      remote_endpoint);
            if (bytes_recv > 192) {
                std::cout << "{\"error\":\"bad packet\", \"reason\":\"wrong size (" << bytes_recv << ")\"\n";
                continue;
            }
            request.resize(bytes_recv);
        }

        std::shared_ptr<DataPingRequestContainer> dp;
        try {
            dp = DataPingRequestContainer::deserialize(request);
            auto reply = datagramReceived(dp, remote_endpoint);
            mSocket->send_to(boost::asio::buffer(reply, reply.size()), remote_endpoint);
        } catch (std::invalid_argument &e) {
            std::cout << "{\"error\":\"bad packet\", \"reason\":\"" << e.what() << ")\"\n";
            continue;
        }

        {
            std::lock_guard<std::mutex> lock(mLock);
            if (!mAlive) {
                std::cout << "dead" << std::endl;
                break;
            }
        }
    }
}

void DataPing::kill() {
    mLock.lock();
    mAlive = false;
    mLock.unlock();
}

std::chrono::milliseconds DataPing::now() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch());
}



std::vector<uint8_t> DataPing::datagramReceived(std::shared_ptr<DataPingRequestContainer> &request, udp::endpoint &endpoint) {
    auto current_ts = DataPing::now();
    validateDataPing(request, current_ts, endpoint.address().to_string());
    DataPingResponseV2 response = DataPingResponseV2();
    response.mVersion = 2;
    response.mTimestamp = current_ts.count();
    response.mSeq = request->seq();
    std::string ident_seq = (boost::format("%1% - %2%") % request->identOrToken() % request->seq()).str();
    std::string ident_ip = (boost::format("%1%%2%") % request->identOrToken() % endpoint.address().to_string()).str();

    uint8_t raw_sig[crypto_sign_BYTES];
    uint8_t msg[ident_ip.length() + 1];
    std::memcpy(msg, ident_ip.data(), ident_ip.length());
    crypto_sign_detached(raw_sig, NULL, msg, ident_ip.length(), mSigSk);
    sodium_bin2base64(response.mSignature, 89, raw_sig, crypto_sign_BYTES, sodium_base64_VARIANT_ORIGINAL);
    size_t count;
    {
        std::lock_guard<std::mutex> lock(*mIncompleteClaim);
        count = mIncomplete->count(ident_seq);
        if (count == 0 and request->type() == eCInit) {
            (*mIncomplete)[ident_seq] = std::make_shared<DataPingState>();
            count = 1;
        }
    }
    if (count > 0) {
        std::shared_ptr<DataPingState> dps = (*mIncomplete)[ident_seq];
        switch (request->type()) {
            case eCInit:
                response.mType = eSInit;
                dps->mSeq = request->seq();
                dps->mIdent = request->identOrToken();
                dps->mIp = endpoint.address().to_string();
                dps->mTimestampsProbe[0] = request->timestamp();
                dps->mTimestampsBeacon[0] = response.mTimestamp;
                dps->mVersion = request->version();
                dps->mState = eS0;
                break;
            case eCSecond:
                response.mType = eSNext;
                dps->mTimestampsProbe[1] = request->timestamp();
                dps->mTimestampsBeacon[1] = response.mTimestamp;
                dps->mState = eS1;
                break;
            case eCFinal:
                response.mType = eSFinal;
                dps->mTimestampsProbe[2] = request->timestamp();
                dps->mTimestampsBeacon[2] = response.mTimestamp;
                dps->mState = eComplete;
                {
                    std::scoped_lock lock(*mIncompleteClaim, *mCompleteClaim);
                    mComplete->insert(std::pair<timestamp_millisec_t, std::shared_ptr<DataPingState>>(
                            (dps->mTimestampsProbe[0] - (dps->mTimestampsProbe[0] % 1000)), dps));
                    mIncomplete->erase(ident_seq);
                }
                break;
        }
    } else {
        response.mType = eSNull;
    }
    return response.serialize();
}

void DataPing::validateDataPing(std::shared_ptr<DataPingRequestContainer> &dp, std::chrono::milliseconds &aCurrentTs, std::string aSourceIp) {
    std::chrono::milliseconds request_ts(dp->timestamp());
    if ((request_ts < aCurrentTs - (24h)) || (request_ts > aCurrentTs + (24h))) {
        std::cout << dp->timestamp() << " - " << aCurrentTs.count() << std::endl;
        throw std::invalid_argument("bad timestamp");
    }
    if (dp->version() == 3) {
        char b64_token[LATENCYGG_TOKEN_LEN];
        std::memcpy(b64_token, dp->identOrToken(), LATENCYGG_TOKEN_LEN);
        unsigned char raw_token[LATENCYGG_TOKEN_DECODED_LEN];
        size_t actual_len;
        sodium_base642bin(raw_token,
                          112, b64_token, 161, NULL, &actual_len, NULL, sodium_base64_VARIANT_ORIGINAL);
        unsigned char decrypted[actual_len - crypto_box_SEALBYTES +1];
        if (crypto_box_seal_open(decrypted, raw_token, actual_len, this->mEncPk, this->mEncSk) != 0) {
            throw std::invalid_argument("bad token");
        }
        std::shared_ptr<DataPingTokenV1> token = DataPingTokenV1::decode(decrypted);
        if (token->mIp != aSourceIp) {
            throw std::invalid_argument((boost::format("source IP doesn't match! Token claims %1%, but I received %2%") % token->mIp % aSourceIp).str().c_str());
        }
        std::chrono::milliseconds token_ts(token->mTimestamp);
        if ((token_ts < aCurrentTs - (60s)) || (token_ts > aCurrentTs + (60s))) {
            throw std::invalid_argument((boost::format("token timestamp out of range! Token claims %1%, but current time is %2%") % token->mTimestamp % aCurrentTs.count()).str().c_str());
        }
    } else if (dp->version() == 2) {
        std::cout << "WARNING: Deprecated packet version received" << std::endl;
    }
}


