//
// Created by tim on 28/09/2021.
//


#include <boost/format.hpp>
#include <chrono>
#include <iostream>
#include "api_client.h"

using namespace LatencyGG::Beacon;
using namespace std::chrono;

static size_t ResponseCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *) userp)->append((char *) contents, size * nmemb);
    return size * nmemb;
}

void ApiClient::registerBeacon(std::string &location, std::string &provider, std::string &ipv4, std::string &ipv6, std::string publickey_b64) {
    auto kurl = curl_easy_init();
    nlohmann::json jdata;
    jdata["location"] = location;
    jdata["provider"] = provider;
    jdata["publickey"] = publickey_b64;
    jdata["version"] = "v1.0.12";
    jdata["ipv4"] = ipv4;
    if (!ipv6.empty()) {
        jdata["ipv6"] = ipv6;
    }
    CURLcode res;
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, (boost::format("%1%%2%") % "Authorization: " % mToken).str().c_str());
    headers = curl_slist_append(headers, "Content-Type: application/json");
    curl_easy_setopt(kurl, CURLOPT_URL, (boost::format("%1%/%2%") % mApi % "register").str().c_str());
    curl_easy_setopt(kurl, CURLOPT_HTTPHEADER, headers);
    std::string jsonBuffer = jdata.dump();
    curl_easy_setopt(kurl, CURLOPT_POSTFIELDS, jsonBuffer.c_str());
    std::string resultBuffer;
    curl_easy_setopt(kurl, CURLOPT_WRITEFUNCTION, ResponseCallback);
    curl_easy_setopt(kurl, CURLOPT_WRITEDATA, &resultBuffer);
    res = curl_easy_perform(kurl);
    if (res != CURLE_OK) {
        curl_easy_cleanup(kurl);
        throw std::runtime_error("unable to register beacon (protocol error)");
    } 
    long http_code = 0;
    curl_easy_getinfo (kurl, CURLINFO_RESPONSE_CODE, &http_code);
    if ((http_code >= 200) && (http_code < 205)) {
        curl_easy_cleanup(kurl);
        throw std::runtime_error("unable to register beacon (authentication error)");
    }
    curl_slist_free_all(headers);
    try {
        auto result = nlohmann::json::parse(resultBuffer);
        mUuid = result["beacon"];
        curl_easy_cleanup(kurl);
    } catch (nlohmann::detail::parse_error &e) {
        std::cout << "unable to parse JSON response, dumping complete response here:\n";
        std::cout << resultBuffer << std::endl;
        curl_easy_cleanup(kurl);
        throw std::runtime_error("unable to register beacon (json parsing error)");
    }
}

void ApiClient::job() {
    while (true) {
        std::this_thread::sleep_for(seconds(mPushPeriod));
        std::vector<nlohmann::json> records;
        {
            std::lock_guard<std::mutex> lock_temp_storage(*mCompleteClaim);
            for (auto &&[key, dps]: *mComplete) {
                try {
                    records.push_back(dps->AsJSON());
                } catch (std::out_of_range &e) {
                    std::cout << "skipping measurement with nonsensical timestamp" << std::endl;
                    continue;
                }
            }
        }
        nlohmann::json jdata(records);
        CURLcode res;
        struct curl_slist *headers = NULL;
        headers = curl_slist_append(headers,
                                    (boost::format("%1%%2%") % "Authorization: " % mToken).str().c_str());
        headers = curl_slist_append(headers, "Content-Type: application/json");
        curl_easy_setopt(mCurl, CURLOPT_URL, (boost::format("%1%/%2%/%3%") % mApi % "record" % mUuid).str().c_str());
        curl_easy_setopt(mCurl, CURLOPT_HTTPHEADER, headers);
        std::string jsonBuffer = jdata.dump();
        curl_easy_setopt(mCurl, CURLOPT_POSTFIELDS, jsonBuffer.c_str());
        res = curl_easy_perform(mCurl);
        if (res != CURLE_OK) {
            std::cout << "API error: (protocol)" << std::endl;
            continue;
        } 
        long http_code = 0;
        curl_easy_getinfo (mCurl, CURLINFO_RESPONSE_CODE, &http_code);
        if ((http_code >= 200) && (http_code < 205)) {
            mComplete->clear();
        } else {
            std::cout << "API error: " << http_code << std::endl;
            continue;
        }
        curl_slist_free_all(headers);
        {
            std::lock_guard<std::mutex> lock_killswitch(mLock);
            if (!mAlive) {
                std::cout << "dead" << std::endl;
                break;
            }
        }
    }
}

void ApiClient::run() {
    mLock.lock();
    mCurl = curl_easy_init();
    mAlive = true;
    mLock.unlock();
    mThread = std::thread(&ApiClient::job, this);
}

void ApiClient::kill() {
    mLock.lock();
    mAlive = false;
    curl_easy_cleanup(mCurl);
    mLock.unlock();
}
