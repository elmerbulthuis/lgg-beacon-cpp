SHELL:=$(PREFIX)/bin/sh

DISTRO?=ubuntu-any
GIT_TAG?=v0.0.0-local

rebuild: clean build

build: out/beacon_${DISTRO}_${GIT_TAG}.deb

clean:
	rm -rf .tmp .build out

out/beacon_${DISTRO}_${GIT_TAG}.deb: .tmp/${DISTRO}
	mkdir -p $(@D)
	dpkg-deb -b $< $@

.build: src/*
	cmake -B $@

.build/release/beacon: .build
	cmake --build $<

.tmp/${DISTRO}: packages/${DISTRO} .build/release/beacon
	mkdir -p $(@D)
	cp -r $< $@
	mkdir -p $@/usr/bin/
	cp $(word 2,$^) $@/usr/bin/
	sed -i 's/Version:.*/Version: '$(patsubst v%,%,${GIT_TAG})'/' $@/DEBIAN/control

.PHONY: build rebuild clean
